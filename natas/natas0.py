#!/usr/bin/env python
import requests as rq
import re, sys

usr = 'natas0'
pswd = 'natas0'

url = 'http://{}.natas.labs.overthewire.org'.format(usr)
res = rq.get(url, auth = (usr, pswd)).text
flag = re.findall('natas1 is (.*) -->', res)[0]

print(flag)
